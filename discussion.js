// CRUD Operations

// Insert Documents (CREATE)

/*
	Syntax:
		Insert One Document
			-db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB",
			});

		Insert Many Documents
			-db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB",
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB",
				}

			])
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
})

db.users.insertMany([
{
	"firstName": "Stephen",
	"lastName": "Hawking",
	"age": 76,
	"email": "stephenhawking@mail.com",
	"department": "none"
},{
	"firstName": "Neil",
	"lastName": "Armstrong",
	"age": 82,
	"email": "neilarsmtrong@mail.com",
	"department": "none"
}

// 1. Make a new collection with the name "courses"
//         2. Insert the following fields and values

//             name: Javascript 101
//             price: 5000
//             description: Introduction to Javascript
//             isActive: true

//             name: HTML 101
//             price: 2000
//             description: Introduction to HTML
//             isActive: true

//             name: CSS 101
//             price: 2500
//             description: Introduction to CSS
//             isActive: false

	])

db.courses.insertMany([
{
	"name": "Javascript 101",
	"price": 5000,
	"desciption": "Introduction to Javascript",
	"isActive": true
},
{
	"name": "HTML 101",
	"price": 2000,
	"desciption": "Introduction to HTML",
	"isActive": true
},
{
	"name": "CSS 101",
	"price": 2500,
	"desciption": "Introduction to CSS",
	"isActive": false
}



	])

// FIND DOCUMENTS (READ)

/*
	Syntax:
		-db.collectionName.find() - this will retrieve all our documents
		-db.collectionName.find({"criteria":"value"}) -will retreive all documents that will match criteria
		-dbcollectionName.findOne({"criteria": "Value"})-will retun the first document in our collection that match the criteria
		-db.collectionName.findOne({}) - will return the first document in our collection

*/

db.users.find();

db.users.find({
	"firstName": "Jane"

})

db.users.find({
	"firstName": "Jane",
	"age": 82

})


// Updating Documents(UPDATE)

/*
	Syntax
		db.collectionName.updateOne({
			"criteria": "field"
		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		})


		db.collectionName.updateMany({
			"criteria":"value"
		},{
		
			$set: {
					"fieldToBeUpdated": "updatedValue"
			}
		})



*/

db.users.insertOne({
	"firstName": "test",
	"lastName": "test",
	"age": 0,
	"email": "test@mail.com",
	"department": "none"

})


// Updating one document

db.users.updateOne(
{
	"firstName": "test"

},
{
	$set: {
		"firstName" : "Bill",
		"lastName": "Gates",
		"age": 65,
		"email": "billgates@mail.com",
		"department": "operations",
		"status": "active"
	}
})
// \\\REmoving a filed
db.users.updateOne(
{
	"firstName": "Bill"
},
{
	$unset: {
			"status": "active"
	}
})

/*
	mini activity
	1. courses collection, update the HTML 101 course
		-makeisactive to false
	2. Add enrollees field to all the documents in our corses collections
		-enrollees: 10
*/


db.courses.updateOne({
	"name": "HTML 101"
},{
	$set: {
		"isActive": false
	}
})

db.courses.updateMany({},
{
	$set: {
		"enrollees": 10
	}
})